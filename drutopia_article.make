api = 2
core = 8.x

projects[ctools][version] = 3.0-alpha27
projects[ctools][subdir] = contrib
projects[pathauto][version] = 1.0-beta1
projects[pathauto][subdir] = contrib
projects[token][version] = 1.0-beta2
projects[token][subdir] = contrib
